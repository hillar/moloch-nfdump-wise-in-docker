#!/bin/sh

env

until curl -sS "http://$ES_HOST:$ES_PORT/_cluster/health?wait_for_status=yellow"
do
    echo "Waiting for ES to start"
    sleep 3
done
echo

# create config.ini with Configure
if [ ! -f $MOLOCHDIR/etc/config.ini ]; then
        echo "running Configure"
	echo $(date) >  $MOLOCHDIR/configured
  cd $MOLOCHDIR/etc/
	[ -f config.ini.sample ] || curl -s -C - -O "https://raw.githubusercontent.com/aol/moloch/master/release/config.ini.sample"
  ls -tlah $MOLOCHDIR/etc/
	Configure
	ls -tlah $MOLOCHDIR/etc/
	echo INIT | $MOLOCHDIR/db/db.pl http://$ES_HOST:$ES_PORT init
  moloch_add_user.sh admin "Admin User" $MOLOCH_PASSWORD --admin
	echo "  user: admin"
	echo "  password: $MOLOCH_PASSWORD"
  if [ ! -z $WISE_HOST ]; then
    echo '# wise conf' >> $MOLOCHDIR/etc/config.ini
    echo 'plugins=wise.so' >> $MOLOCHDIR/etc/config.ini
    echo "wiseHost=${WISE_HOST}:${WISE_PORT}" >> $MOLOCHDIR/etc/config.ini
  fi
else
	echo "using existing config.ini"
  echo
	grep -v "#" $MOLOCHDIR/etc/config.ini | grep .
	echo

fi

# INIT DB
if [ "$INITALIZEDB" = "true" ] ; then
	echo 'INIT .. '
	echo INIT | $MOLOCHDIR/db/db.pl http://$ES_HOST:$ES_PORT init
	moloch_add_user.sh admin "Admin User" $MOLOCH_PASSWORD --admin
	echo "  user: admin"
        echo "  password: $MOLOCH_PASSWORD"
fi

# WIPE DB
if [ "$WIPEDB" = "true" ]; then
        echo 'WIPE .. '
        echo WIPE | $MOLOCHDIR/db/db.pl http://$ES_HOST:$ES_PORT wipe
fi


if [ "$CAPTURE" = "on" ]
then
    echo "Start capture..."
    moloch-capture >> $MOLOCHDIR/logs/capture.log 2>&1 &
fi

if [ "$MONITOR" = "on" ]
then
    echo "Start monitoring $SPOOLDIR ..."
    moloch-capture -R $SPOOLDIR -m --delete --copy >> $MOLOCHDIR/logs/monitor.log 2>&1 &
fi

if [ "$VIEWER" = "on" ]
then
   echo "Starting viewer ..."
   /bin/sh -c 'cd $MOLOCHDIR/viewer; $MOLOCHDIR/bin/node viewer.js -c $MOLOCHDIR/etc/config.ini | grep -v 'eshealth.json'| tee -a $MOLOCHDIR/logs/viewer.log 2>&1'
fi
