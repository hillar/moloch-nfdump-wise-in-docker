#!/bin/bash                                                                                                                           

echo "$(date) start" >> /var/log/pcaps-watch.log                                                                                      

cd /var/spool/pcaps                                                                                                                   
inotifywait -m -r -e close_write --format %w%f . | while read file; do                                                                
  echo "$(date) ${file}" >> /var/log/pcaps-watch.log                                                                                  
  /root/scripts/convert-snoop.bash "$(pwd)" "${file}" &                                                                               
done                                                                                                                                  

echo "$(date) reload" >> /var/log/pcaps-watch.log                                                                                     
cd /root/scripts                                                                                                                      
$0                                 